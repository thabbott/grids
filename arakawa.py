"""
Arakawa-type computational grids
"""

import numpy as np
import matplotlib.pyplot as plt

class C1(object):
    """
    1-d Arakawa C-like staggered grid
    """

    def __init__(self, x, tracer = False):
        """
        Parameters
        ----------
        x: array-like
            Grid points

        tracer: bool, optional
            whether x contains tracer or velocity points. Default: False (x
            contains velocity points)
        """
        if not tracer:
            self.xu = np.array(x)
            self.xq = 0.5*(self.xu[1:] + self.xu[0:-1])
            self.xq = np.append(self.xq, 2*self.xu[-1] - self.xq[-1])
        else:
            self.xq = np.array(x)
            self.xu = 0.5*(self.xq[1:] + self.xq[0:-1])
            self.xu = np.append(self.xu, 2*self.xq[-1] - self.xu[-1])

    def q(self):
        """
        Create an array for holding values at tracer points

        Returns
        -------
        array-like:
            Tracer array
        """
        return np.zeros(self.xq.shape)

    def u(self):
        """
        Create an array for holding values at velocity points

        Returns
        -------
        array-like:
            Velocity array
        """
        return np.zeros(self.xu.shape)

    def show(self):
        """
        Create a plot of the grid

        Return
        ------
        matplotlib.pyplot.figure:
            Plot of the grid
        """
        aspect = np.diff(self.xq).max() / \
                (self.xq.max() - self.xq.min())
        f = plt.figure(figsize = (10, aspect*10))
        plt.plot(self.xu, np.zeros(self.xu.shape), 'kx')
        plt.plot(self.xq, np.zeros(self.xq.shape), 'ko')
        plt.xlim([min(self.xq.min(), self.xu.min()) -
                    np.abs(self.xq.min() - self.xu.min()),
                  max(self.xq.max(), self.xu.max()) +
                    np.abs(self.xq.max() - self.xu.max())])
        plt.ylim([-0.5, 0.5])
        plt.yticks([])
        plt.title('x: velocity points; o: tracer points')
        plt.xlabel('x')
        return f
