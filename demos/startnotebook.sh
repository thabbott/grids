#!/bin/bash

# Set search path before starting notebook
export PYTHONPATH="..:$PYTHONPATH"
jupyter notebook
